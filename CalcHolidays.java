package com.nolessan;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class CalcHolidays {

    static SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");

    static String newYears;
    static String mlkDay;
    static String presidentsDay;
    static String memorialDay;
    static String independenceDay;
    static String laborDay;
    static String veteransDay;
    static String thanksgiving;
    static String christmasDay;

    static Calendar newYearsCal;
    static Calendar mlkDayCal;
    static Calendar presidentsDayCal;
    static Calendar memorialDayCal;
    static Calendar independenceDayCal;
    static Calendar laborDayCal;
    static Calendar veteransDayCal;
    static Calendar thanksgivingCal;
    static Calendar christmasDayCal;


    public static void setYear(int year) {
        newYears = df.format(checkIfSunday(stringToCal(year + "-01-01")).getTime());
        mlkDay = getHolidateAsString(year, 0, 3, Calendar.MONDAY);
        presidentsDay = getHolidateAsString(year, 1, 3, Calendar.MONDAY);
        memorialDay = getHolidateAsString(year, 4, 5, Calendar.MONDAY);
        independenceDay = df.format(checkIfSunday(stringToCal(year + "-07-04")).getTime());
        laborDay = getHolidateAsString(year, 8, 1, Calendar.MONDAY);
        veteransDay = df.format(checkIfSunday(stringToCal(year + "-11-11")).getTime());
        thanksgiving = getHolidateAsString(year, 10, 4, Calendar.THURSDAY);
        christmasDay = df.format(checkIfSunday(stringToCal(year + "-12-25")).getTime());

        newYearsCal = new GregorianCalendar(year, 0, 01);
        newYearsCal = checkIfSunday(newYearsCal);
        mlkDayCal = getHolidateAsCalendar(year, 0, 3, Calendar.MONDAY);
        presidentsDayCal = getHolidateAsCalendar(year, 1, 3, Calendar.MONDAY);
        memorialDayCal = getHolidateAsCalendar(year, 4, 5, Calendar.MONDAY);
        independenceDayCal = new GregorianCalendar(year, 6, 4);
        independenceDayCal = checkIfSunday(independenceDayCal);
        laborDayCal = getHolidateAsCalendar(year, 8, 1, Calendar.MONDAY);
        veteransDayCal = new GregorianCalendar(year, 10, 11);
        veteransDayCal = checkIfSunday(veteransDayCal);
        thanksgivingCal = getHolidateAsCalendar(year, 10, 4, Calendar.THURSDAY);
        christmasDayCal = new GregorianCalendar(year, 11, 25);
        christmasDayCal = checkIfSunday(christmasDayCal);
    }

    // returns date string in SQL format YYYY-MM-DD
    public static String getHolidateAsString(int year, int month, int weekOfMonth, int dayOfWeek) {
        Calendar cal = new GregorianCalendar();
        cal.clear();

        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.MONTH, month);
        cal.set(Calendar.DAY_OF_WEEK, dayOfWeek);

        // accommodate for Memorial Day "last Monday in May" rule, depending on year this could be week 4 or week 5
        if (weekOfMonth == 5) {
            weekOfMonth = cal.getActualMaximum(Calendar.DAY_OF_WEEK_IN_MONTH);
        }

        cal.set(Calendar.DAY_OF_WEEK_IN_MONTH, weekOfMonth);

        String dateString = df.format(cal.getTime());

        return dateString;
    }

    // returns calendar object of holiday
    public static Calendar getHolidateAsCalendar(int year, int month, int weekOfMonth, int dayOfWeek) {
        Calendar cal = new GregorianCalendar();
        cal.clear();


        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.MONTH, month);
        cal.set(Calendar.DAY_OF_WEEK, dayOfWeek);

        // accommodate for Memorial "last Monday in May" rule, depending on year this could be week 4 or week 5
        if (weekOfMonth == 5) {
            weekOfMonth = cal.getActualMaximum(Calendar.DAY_OF_WEEK_IN_MONTH);
        }

        cal.set(Calendar.DAY_OF_WEEK_IN_MONTH, weekOfMonth);

        cal = checkIfSunday(cal);

        return cal;
    }

    // takes in constant date holidays, converts to cal
    public static Calendar stringToCal(String dateString) {
        Calendar cal = new GregorianCalendar();
        cal.clear();
        cal.set(Integer.parseInt(dateString.substring(0, 4)), Integer.parseInt(dateString.substring(5, 7)) - 1,
                Integer.parseInt(dateString.substring(8, 10)));
        return cal;
    }

    /*checks if holiday falls on Sunday, if so increments date to day of observance for Federal Reserve offices
    see: http://www.federalreserve.gov/aboutthefed/k8.htm */
    public static Calendar checkIfSunday(Calendar cal) {
        if (cal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
            int newDayOfMonth = cal.get(Calendar.DAY_OF_MONTH);
            cal.set(Calendar.DAY_OF_MONTH, ++newDayOfMonth);
        }
        return cal;
    }


    public static String calDayOfWeekToString(Calendar cal) {
        switch (cal.get(Calendar.DAY_OF_WEEK)) {
            case 1:
                return "Sunday";
            case 2:
                return "Monday";
            case 3:
                return "Tuesday";
            case 4:
                return "Wednesday";
            case 5:
                return "Thursday";
            case 6:
                return "Friday";
            case 7:
                return "Saturday";

        }
        return null;
    }

    public static ArrayList<Object> getList(int year) {
        setYear(year);

        return null;

    }

    public static void printDetails() {

        System.out.println("New Years: \r\n  " + newYears + "\r\n  Observed on: " + calDayOfWeekToString(newYearsCal));
        System.out.println("MLK Day: \r\n  " + mlkDay + "\r\n  Observed on: " + calDayOfWeekToString(mlkDayCal));
        System.out.println("Presidents Day: \r\n  " + presidentsDay + "\r\n  Observed on: " + calDayOfWeekToString(presidentsDayCal));
        System.out.println("Memorial Day: \r\n  " + memorialDay + "\r\n  Observed on: " + calDayOfWeekToString(memorialDayCal));
        System.out.println("Independence Day: \r\n  " + independenceDay + "\r\n  Observed on: " + calDayOfWeekToString(independenceDayCal));
        System.out.println("Labor Day: \r\n  " + laborDay + "\r\n  Observed on: " + calDayOfWeekToString(laborDayCal));
        System.out.println("Veterans Day: \r\n  " + veteransDay + "\r\n  Observed on: " + calDayOfWeekToString(veteransDayCal));
        System.out.println("Thanksgiving: \r\n  " + thanksgiving + "\r\n  Observed on: " + calDayOfWeekToString(thanksgivingCal));
        System.out.println("Christmas: \r\n  " + christmasDay + "\r\n  Observed on: " + calDayOfWeekToString(christmasDayCal));
    }

}