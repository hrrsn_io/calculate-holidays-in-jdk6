package com.nolessan;

import java.util.Scanner;

public class Main {

    @SuppressWarnings("resource")
    public static void main(String[] args) {

        System.out.println("Enter year:");
        Scanner in = new Scanner(System.in);

        int yearFromInput = in.nextInt();

        CalcHolidays.setYear(yearFromInput);

        CalcHolidays.printDetails();


    }

}